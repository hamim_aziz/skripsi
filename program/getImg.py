import cv2
import numpy as np
import math

ksize = 5
k = math.sqrt(2)
sigma = k/2
img = cv2.imread('img.jpeg')

height, width = img.shape[:2]

img = cv2.resize(img, (int(width/3), int (height/3)), interpolation = cv2.INTER_CUBIC)

img1 = img.copy()
# Convert Gray Scale
level_1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
cv2.imwrite("level_1.jpg", level_1)

# Normalisasi
level_1 = cv2.normalize(level_1, level_1.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
# Gaussian Kernel
GaussianKernel0 = cv2.getGaussianKernel(ksize, sigma, cv2.CV_32F)
GaussianKernel1 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 1), cv2.CV_32F)
GaussianKernel2 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 2), cv2.CV_32F)
GaussianKernel3 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 3), cv2.CV_32F)
GaussianKernel4 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 4), cv2.CV_32F)
GaussianKernel5 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 5), cv2.CV_32F)
GaussianKernel6 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 6), cv2.CV_32F)
GaussianKernel7 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 7), cv2.CV_32F)
GaussianKernel8 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 8), cv2.CV_32F)
GaussianKernel9 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 9), cv2.CV_32F)
GaussianKernel10 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 10), cv2.CV_32F)
GaussianKernel = cv2.getGaussianKernel(ksize, sigma*1.5, cv2.CV_32F)

# Gauss Image Level 1
gauss1_level1 = cv2.filter2D(level_1, -1, GaussianKernel0)
gauss2_level1 = cv2.filter2D(level_1, -1, GaussianKernel6)

cv2.imshow("gauss1_level1.jpg", gauss1_level1)
cv2.imshow("gauss2_level1.jpg", gauss2_level1)

## DoG Level 1
dog1_level1 = level_1.copy()
for y in range(dog1_level1.shape[0]):
     for x in range(dog1_level1.shape[1]):
          dog1_level1[y][x] = gauss1_level1[y][x] - gauss2_level1[y][x]

cv2.imshow("dog1_level1.jpg", dog1_level1)

cv2.waitKey(0)
cv2.destroyAllWindows()
