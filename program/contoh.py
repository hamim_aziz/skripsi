import cv2
import numpy as np
import math
import pickle

ksize = 5
k = math.sqrt(2)
sigma = k/2
img = cv2.imread('img.jpeg')

height, width = img.shape[:2]
img = cv2.resize(img, (int(width/2), int (height/2)), interpolation = cv2.INTER_CUBIC)
img1 = img.copy()
cv2.imshow("Img", img1)

level_1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
level_1 = cv2.normalize(level_1, level_1.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
cv2.imshow("Gray", level_1)

GaussianKernel0 = cv2.getGaussianKernel(ksize, sigma, cv2.CV_32F)
GaussianKernel1 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 1), cv2.CV_32F)
GaussianKernel2 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 2), cv2.CV_32F)
GaussianKernel3 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 3), cv2.CV_32F)
GaussianKernel4 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 4), cv2.CV_32F)
GaussianKernel5 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 5), cv2.CV_32F)
GaussianKernel6 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 6), cv2.CV_32F)
GaussianKernel7 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 7), cv2.CV_32F)
GaussianKernel8 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 8), cv2.CV_32F)
GaussianKernel9 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 9), cv2.CV_32F)
GaussianKernel10 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 10), cv2.CV_32F)

gauss1_level1 = cv2.filter2D(level_1, -1, GaussianKernel0)
gauss2_level1 = cv2.filter2D(level_1, -1, GaussianKernel1)
gauss3_level1 = cv2.filter2D(level_1, -1, GaussianKernel2)
gauss4_level1 = cv2.filter2D(level_1, -1, GaussianKernel0)
gauss5_level1 = cv2.filter2D(level_1, -1, GaussianKernel10)
cv2.imshow("gauss", gauss5_level1)

dog1_level1 = level_1.copy()
dog2_level1 = level_1.copy()
coba = level_1.copy()
for y in range(dog1_level1.shape[0]):
     for x in range(dog1_level1.shape[1]):
          dog1_level1[y][x] = gauss1_level1[y][x] - gauss2_level1[y][x]
          dog2_level1[y][x] = gauss2_level1[y][x] - gauss3_level1[y][x]
          coba[y][x] = gauss4_level1[y][x] - gauss5_level1[y][x]

cv2.imshow("coba", coba)

##k1, c1, kp1 = find_keypoint(dog1_level1, dog2_level1, dog3_level1, gauss1, img1, ksize)
cv2.waitKey(0)
cv2.destroyAllWindows()
