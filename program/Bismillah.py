from timeit import default_timer as timer
start = timer()
import cv2
import numpy as np
import math
import sys

def getExtrema(dog1, dog2, dog3, o, s):
     t = len(dog1)
     l = len(dog1[0])
     extrema = []
     for m in range(2, t-2):
          for n in range(2, l-2):
               atas = np.array(dog1[m-1:m+2, n-1:n+2])
               tengah = np.array(dog2[m-1:m+2, n-1:n+2])
               bawah = np.array(dog3[m-1:m+2, n-1:n+2])
               maxima = False
               minima = False
               if dog2[m][n] > atas.max() and dog2[m][n] > bawah.max() and dog2[m][n] == tengah.max():
                    maxima = True
               elif dog2[m][n] < atas.min() and dog2[m][n] < bawah.min() and dog2[m][n] == tengah.min():
                    minima = True
               if maxima or minima:
                    extrema.append([o, s, m, n])
     return extrema

def getHessianMatrix(dog, m, n):
     h11 = dog[m+1][n] + dog[m-1][n] - (2 * dog[m][n])
     h22 = dog[m][n+1] + dog[m][n-1] - (2 * dog[m][n])
     h12 = (dog[m+1][n+1] + dog[m-1][n-1] - dog[m+1][n-1] - dog[m-1][n+1])/4
     H = [[h11, h12], [h12, h22]]
     return H

def quadratic_interpolation(o, s, m, n):
#     print(o, s, m, n)
     o, s, m, n = int(o), int(s), int(m), int(n)
     if s <= 1:
          s = 2
     if s >= 5:
          s = 4
     G = [
          (globals()['dog' + str(s+1) + "_level" + str(o)][m][n] - globals()['dog' + str(s-1) + "_level" + str(o)][m][n])/2,
          (globals()['dog' + str(s) + "_level" + str(o)][m+1][n] - globals()['dog' + str(s) + "_level" + str(o)][m-1][n])/2,
          (globals()['dog' + str(s) + "_level" + str(o)][m][n+1] - globals()['dog' + str(s) + "_level" + str(o)][m][n-1])/2
          ]
     h11 = (globals()['dog' + str(s+1) + "_level" + str(o)][m][n] + globals()['dog' + str(s-1) + "_level" + str(o)][m][n] - 
            (2 * globals()['dog' + str(s) + "_level" + str(o)][m][n]))
     h22 = (globals()['dog' + str(s) + "_level" + str(o)][m+1][n] + globals()['dog' + str(s) + "_level" + str(o)][m-1][n] - 
            (2 * globals()['dog' + str(s) + "_level" + str(o)][m][n]))
     h33 = (globals()['dog' + str(s) + "_level" + str(o)][m][n+1] + globals()['dog' + str(s) + "_level" + str(o)][m][n-1] - 
            (2 * globals()['dog' + str(s) + "_level" + str(o)][m][n]))
     h12 = (globals()['dog' + str(s+1) + "_level" + str(o)][m+1][n] + 
            globals()['dog' + str(s-1) + "_level" + str(o)][m-1][n] - 
            globals()['dog' + str(s+1) + "_level" + str(o)][m-1][n] - 
            globals()['dog' + str(s-1) + "_level" + str(o)][m+1][n])/4
     h13 = (globals()['dog' + str(s+1) + "_level" + str(o)][m][n+1] + 
            globals()['dog' + str(s-1) + "_level" + str(o)][m][n-1] - 
            globals()['dog' + str(s+1) + "_level" + str(o)][m][n-1] - 
            globals()['dog' + str(s-1) + "_level" + str(o)][m][n+1])/4
     h23 = (globals()['dog' + str(s) + "_level" + str(o)][m+1][n+1] + 
            globals()['dog' + str(s) + "_level" + str(o)][m-1][n-1] - 
            globals()['dog' + str(s) + "_level" + str(o)][m+1][n-1] - 
            globals()['dog' + str(s) + "_level" + str(o)][m-1][n+1])/4
     H = [
          [h11, h12, h13],
          [h12, h22, h23],
          [h13, h23, h33]
          ]
     
     if np.linalg.cond(H) < 1/sys.float_info.epsilon:          
          alfa = -1 * np.matmul(np.linalg.inv(H), G)
          Gt = np.array(G)[ : , np.newaxis]
          Gt = [x/2 for x in Gt]
          omega = globals()['dog' + str(s) + "_level" + str(o)][m][n] - np.matmul(np.matmul(np.linalg.inv(H), G), Gt)
          return True, alfa, omega
     else:
          return False, [], []

ksize = 5
k = math.sqrt(2)
img = cv2.imread('Train/4.jpg')

height, width = img.shape[:2]

img = cv2.resize(img, (int(width/5), int (height/5)), interpolation = cv2.INTER_CUBIC)
kernel_input = cv2.getGaussianKernel(ksize, 0.5, cv2.CV_32F)

img1 = img.copy();
img2 = cv2.resize(img1, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
img3 = cv2.resize(img2, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
img4 = cv2.resize(img3, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)

img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = cv2.normalize(img, img.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
img = cv2.filter2D(img, -1, kernel_input)


level_1 = img.copy()
level_2 = cv2.resize(level_1, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
level_3 = cv2.resize(level_2, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
level_4 = cv2.resize(level_3, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)

current = timer()
print("1. Read image, resize, convert gray scale e.t : %.2f Sec" % (current-start))
time = timer()

sigma = [0.8, 1.01, 1.27, 1.6, 2.02, 2.54, 3.2, 4.03, 5.08, 6.4, 8.06, 10.16, 12.8, 16.13, 20.32, 25.6, 32.25, 40.64]
sigma_min = 0.8
delta = [0.5, 1, 2, 4, 8]

# Gaussian Kernel
kernel = []
for i in range(len(sigma)):
     kernel.append(cv2.getGaussianKernel(ksize, sigma[i], cv2.CV_32F))

# Gauss Image Level 1
gauss1_level1 = cv2.filter2D(level_1, -1, (delta[0]/delta[0]) * sigma_min * (2 ** (0/3)))
gauss2_level1 = cv2.filter2D(level_1, -1, (delta[0]/delta[0]) * sigma_min * (2 ** (1/3)))
gauss3_level1 = cv2.filter2D(level_1, -1, (delta[0]/delta[0]) * sigma_min * (2 ** (2/3)))
gauss4_level1 = cv2.filter2D(level_1, -1, (delta[0]/delta[0]) * sigma_min * (2 ** (3/3)))
gauss5_level1 = cv2.filter2D(level_1, -1, (delta[0]/delta[0]) * sigma_min * (2 ** (4/3)))
gauss6_level1 = cv2.filter2D(level_1, -1, (delta[0]/delta[0]) * sigma_min * (2 ** (5/3)))

# Gauss Image Level 2
gauss1_level2 = cv2.filter2D(level_2, -1, kernel[3])
gauss2_level2 = cv2.filter2D(level_2, -1, kernel[4])
gauss3_level2 = cv2.filter2D(level_2, -1, kernel[5])
gauss4_level2 = cv2.filter2D(level_2, -1, kernel[6])
gauss5_level2 = cv2.filter2D(level_2, -1, kernel[7])
gauss6_level2 = cv2.filter2D(level_2, -1, kernel[8])

# Gauss Image Level 3
gauss1_level3 = cv2.filter2D(level_3, -1, kernel[6])
gauss2_level3 = cv2.filter2D(level_3, -1, kernel[7])
gauss3_level3 = cv2.filter2D(level_3, -1, kernel[8])
gauss4_level3 = cv2.filter2D(level_3, -1, kernel[9])
gauss5_level3 = cv2.filter2D(level_3, -1, kernel[10])
gauss6_level3 = cv2.filter2D(level_3, -1, kernel[11])

# Gauss Image Level 4
gauss1_level4 = cv2.filter2D(level_4, -1, kernel[9])
gauss2_level4 = cv2.filter2D(level_4, -1, kernel[10])
gauss3_level4 = cv2.filter2D(level_4, -1, kernel[11])
gauss4_level4 = cv2.filter2D(level_4, -1, kernel[12])
gauss5_level4 = cv2.filter2D(level_4, -1, kernel[13])
gauss6_level4 = cv2.filter2D(level_4, -1, kernel[14])

current = timer()
print("2. Gaussian Filter e.t : %.2f Sec" % (current-time))
time = timer()

# DoG Level 1
dog1_level1 = level_1.copy()
dog2_level1 = level_1.copy()
dog3_level1 = level_1.copy()
dog4_level1 = level_1.copy()
dog5_level1 = level_1.copy()
for y in range(dog1_level1.shape[0]):
     for x in range(dog1_level1.shape[1]):
          dog1_level1[y][x] = gauss2_level1[y][x] - gauss1_level1[y][x]
          dog2_level1[y][x] = gauss3_level1[y][x] - gauss2_level1[y][x]
          dog3_level1[y][x] = gauss4_level1[y][x] - gauss3_level1[y][x]
          dog4_level1[y][x] = gauss5_level1[y][x] - gauss4_level1[y][x]
          dog5_level1[y][x] = gauss6_level1[y][x] - gauss5_level1[y][x]

# DoG Level 2
dog1_level2 = level_2.copy()
dog2_level2 = level_2.copy()
dog3_level2 = level_2.copy()
dog4_level2 = level_2.copy()
dog5_level2 = level_2.copy()
for y in range(dog1_level2.shape[0]):
     for x in range(dog1_level2.shape[1]):
          dog1_level2[y][x] = gauss2_level2[y][x] - gauss1_level2[y][x]
          dog2_level2[y][x] = gauss3_level2[y][x] - gauss2_level2[y][x]
          dog3_level2[y][x] = gauss4_level2[y][x] - gauss3_level2[y][x]
          dog4_level2[y][x] = gauss5_level2[y][x] - gauss4_level2[y][x]
          dog5_level2[y][x] = gauss6_level2[y][x] - gauss5_level2[y][x]

# DoG Level 3
dog1_level3 = level_3.copy()
dog2_level3 = level_3.copy()
dog3_level3 = level_3.copy()
dog4_level3 = level_3.copy()
dog5_level3 = level_3.copy()
for y in range(dog1_level3.shape[0]):
     for x in range(dog1_level3.shape[1]):
          dog1_level3[y][x] = gauss2_level3[y][x] - gauss1_level3[y][x]
          dog2_level3[y][x] = gauss3_level3[y][x] - gauss2_level3[y][x]
          dog3_level3[y][x] = gauss4_level3[y][x] - gauss3_level3[y][x]
          dog4_level3[y][x] = gauss5_level3[y][x] - gauss4_level3[y][x]
          dog5_level3[y][x] = gauss6_level3[y][x] - gauss5_level3[y][x]

# DoG Level 4
dog1_level4 = level_4.copy()
dog2_level4 = level_4.copy()
dog3_level4 = level_4.copy()
dog4_level4 = level_4.copy()
dog5_level4 = level_4.copy()
for y in range(dog1_level4.shape[0]):
     for x in range(dog1_level4.shape[1]):
          dog1_level4[y][x] = gauss2_level4[y][x] - gauss1_level4[y][x]
          dog2_level4[y][x] = gauss3_level4[y][x] - gauss2_level4[y][x]
          dog3_level4[y][x] = gauss4_level4[y][x] - gauss3_level4[y][x]
          dog4_level4[y][x] = gauss5_level4[y][x] - gauss4_level4[y][x]
          dog5_level4[y][x] = gauss6_level4[y][x] - gauss5_level4[y][x]

current = timer()
print("3. Difference of Gaussian e.t : %.2f Sec" % (current-time))
time = timer()

extrema = []
ex1231 = getExtrema(dog1_level1, dog2_level1, dog3_level1, 1, 2)
ex2341 = getExtrema(dog2_level1, dog3_level1, dog4_level1, 1, 3)
ex3451 = getExtrema(dog3_level1, dog4_level1, dog5_level1, 1, 4)
extrema.extend(ex1231)
extrema.extend(ex2341)
extrema.extend(ex3451)

ex1232 = getExtrema(dog1_level2, dog2_level2, dog3_level2, 2, 2)
ex2342 = getExtrema(dog2_level2, dog3_level2, dog4_level2, 2, 3)
ex3452 = getExtrema(dog3_level2, dog4_level2, dog5_level2, 2, 4)
extrema.extend(ex1232)
extrema.extend(ex2342)
extrema.extend(ex3452)

ex1233 = getExtrema(dog1_level3, dog2_level3, dog3_level3, 3, 2)
ex2343 = getExtrema(dog2_level3, dog3_level3, dog4_level3, 3, 3)
ex3453 = getExtrema(dog3_level3, dog4_level3, dog5_level3, 3, 4)
extrema.extend(ex1233)
extrema.extend(ex2343)
extrema.extend(ex3453)

ex1234 = getExtrema(dog1_level4, dog2_level4, dog3_level4, 4, 2)
ex2344 = getExtrema(dog2_level4, dog3_level4, dog4_level4, 4, 3)
ex3454 = getExtrema(dog3_level4, dog4_level4, dog5_level4, 4, 4)
extrema.extend(ex1234)
extrema.extend(ex2344)
extrema.extend(ex3454)

current = timer()
print("4. Extrema Detection e.t : %.3f Sec" % (current-time))
time = timer()

extrema_contrast = []
for x in range(len(extrema)):
     o = extrema[x][0]
     s = extrema[x][1]
     m = extrema[x][2]
     n = extrema[x][3]
     gambar = 'dog' + str(s) + "_level" + str(o)
     bil = abs(locals()[gambar][m][n])
     if bil >= 0.01:
          extrema_contrast.append(extrema[x])

current = timer()
print("5. Discard Low Contrast e.t : %.3f Sec" % (current-time))
time = timer()

keypoint_interpolation = []
for x in range(len(extrema_contrast)):
     s, m, n = extrema_contrast[x][1], extrema_contrast[x][2], extrema_contrast[x][3]
     o = extrema_contrast[x][0]
     for i in range(10):
          status, alfa, omega = quadratic_interpolation(o, s, m, n)
          if status == True:
               sig = (delta[o]/delta[0])*delta[0]*(2**((alfa[0]+s)/3))
               y, z = delta[o]*(alfa[1]+m), delta[o]*(alfa[1]+n)
               s, m, n = round(s+alfa[0]), round(m+alfa[1]), round(n+alfa[2])
               if s == 0:
                    break
               alfa_max = alfa.max()
               alfa_min = alfa.min()
               if abs(alfa_min) > abs(alfa_max):
                    alfa_max = abs(alfa_min)
               if alfa_max <= 0.5:
                    keypoint_interpolation.append([o, s, m, n, y, z, sig, omega])
                    break
          

current = timer()
print("6. Keypoint Interpolation e.t : %.3f Sec" % (current-time))
time = timer()

keypoint_omega = []
for x in range(len(keypoint_interpolation)):
     if abs(keypoint_interpolation[x][6]) >= 0.015:
          keypoint_omega.append(keypoint_interpolation[x])

current = timer()
print("7. Low Contrast Omega Threshold e.t : %.3f Sec" % (current-time))
time = timer()

keypoint = []
for x in range(len(keypoint_omega)):
     o = int(keypoint_omega[x][0])
     s = int(keypoint_omega[x][1])
     m = int(keypoint_omega[x][2])
     n = int(keypoint_omega[x][3])
     gambar = 'dog' + str(s) + "_level" + str(o)
     H = getHessianMatrix(locals()[gambar], m, n)
     eigvals, eigvec = np.linalg.eig(H)
     det = eigvals[0] * eigvals[1]
     trace = (eigvals[0] + eigvals[1])**2
     r = trace/det
     if r < 10:
          keypoint.append(extrema_contrast[x])

current = timer()
print("8. Discard Edges e.t : %.3f Sec" % (current-time))
time = timer()

for i in keypoint:
     o = i[0]
     s = i[1]
     m = i[2]
     n = i[3]
     gambar = 'dog' + str(s) + "_level" + str(o)
     imgg = 'img' + str(o)
     cv2.circle(locals()[imgg], (n, m), 3, (0, 255, 0), -1)
     cv2.imshow(gambar, locals()[imgg])


end = timer()
print("Total executin time : ", end-start, " Second")

if len(keypoint) != 0:
     cv2.waitKey(0)
     cv2.destroyAllWindows()
     cv2.waitKey(1)
