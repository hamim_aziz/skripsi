from multiprocessing import Process

def multi_run_wrapper(args):
    return local_maxima(*args)

def local_maxima(atas, tengah, bawah):
    pix = tengah[1][1]
    dimensi = len(tengah)
    maxima = True
    for y in atas:
        for x in y:
            if (pix < x) or (pix == x):
                maxima = False
    if maxima == True:
        for y in bawah:
            for x in y:
                if (pix < x) or (pix == x):
                    maxima = False
    if maxima == True:
        for y in range(dimensi):
            for x in range(dimensi):
                if (x != ((dimensi/2)-0.5)) or (y != ((dimensi/2)-0.5)):
                    if (pix < tengah[y][x]) or (pix == tengah[y][x]):
                        maxima = False
    return maxima

