#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 16 11:29:56 2018

@author: macbook
"""

import numpy as np
import cv2
from matplotlib import pyplot as plt
import pickle
from goodreads import client

#cap = cv2.VideoCapture(0)
#
#while(True):
#     ret, frame = cap.read()
#     
#     gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     gray = cv2.GaussianBlur(gray, (3, 3), 0)
#     
#     # detect edges in the image
#     edged = cv2.Canny(gray, 10, 250)
#     
#     kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
#     closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
#     
#     (_, cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#     total = 0
#     
#     # loop over the contours
#     for c in cnts:
#          # approximate the contour
#          peri = cv2.arcLength(c, True)
#          approx = cv2.approxPolyDP(c, 0.14 * peri, True)
#          
#          # if the approximated contour has four points, then assume that the
#          # contour is a book -- a book is a rectangle and thus has four vertices
#          if len(approx) == 4:
#               cv2.drawContours(frame, [approx], -1, (0, 255, 0), 4)
#               total += 1
#     
#
#     cv2.imshow('frame', frame)
##     cv2.imshow('closed', closed)
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#          break
#    
#cap.release()
#cv2.destroyAllWindows()
#cv2.waitKey(1)

f = open('latih.pckl', 'rb')
train = pickle.load(f)
f.close()

for i in range(20):
     img = cv2.imread('datatest rotate 45/' + str(i+1) + '.jpg')

     img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
     
     sift = cv2.xfeatures2d.SIFT_create(nfeatures = 100)
     bf = cv2.BFMatcher()
     
     (kp, des) = sift.detectAndCompute(img, None)
     
     best = 0
     bestMatch = 0
     for index in range(len(train)):
          matches = bf.knnMatch(des, train[index][0], k=2)
          
          good = []
          for m,n in matches:
              if m.distance < 0.75*n.distance:
                  good.append([m])
                    
          if len(good) > best:
               best = len(good)
               bestMatch = index
     
     print("###### ", (i+1), " #####")
     gc = client.GoodreadsClient("ocezYtOZLhHQHoWuaOE4w", "gMGUvirhqfWX8L02pBvdbui3iWX5jvRIeiElFmcHGMw")
     book = gc.book(train[bestMatch][1])
     title = book.title
     rating = book.average_rating
     print((i+1), ". ", title)
     
     
#     img3 = cv2.drawMatchesKnn(img1, kp1, img2, kp2, good, None, flags=2)
     
#     plt.imshow(img3),plt.show()

#id = [35412372, 35069547, 2657, 33229198, 370493, 70401, 7763, 26198109, 2257790, 1582479, 18671087, 18566709, 27153312, 18495590, 26228873, 30754980, 29069374, 35606560, 34828719, 25613472]
#
#data = []
#
#for num in range(4):
#     for bil in range(20):
#          filename = "Train/" + str(bil+(20*num)+1) + ".JPG"
#          img = cv2.imread(filename)
#          img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#          
#          sift = cv2.xfeatures2d.SIFT_create(nfeatures = 100)
#          
#          (kp, des) = sift.detectAndCompute(img, None)
#          
#          data.append([des, id[bil]])
#          
#          print(bil+(20*num)+1)
#
#f = open('latih.pckl', 'wb')
#train = pickle.dump(data, f)
#f.close()
