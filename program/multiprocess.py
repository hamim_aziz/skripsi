from timeit import default_timer as timer
from multiprocessing import Pool
start = timer()
import cv2
import numpy as np
import math
import pickle
from goodreads import client
from lm import *

f = open('store_gelap.pckl', 'rb')
train = pickle.load(f)
f.close()

maxima=False

def local_minima(atas, tengah, bawah):
     pix = tengah[1][1]
     dimensi = len(tengah)
     minima = True
     for y in atas:
          for x in y:
               if (pix > x) or (pix == x):
                    minima = False
     if minima == True:
          for y in bawah:
               for x in y:
                    if (pix > x) or (pix == x):
                         minima = False
     if minima == True:
          for y in range(dimensi):
               for x in range(dimensi):
                    if (x != ((dimensi/2)-0.5)) or (y != ((dimensi/2)-0.5)):
                         if (pix > tengah[y][x]) or (pix == tengah[y][x]):
                              minima = False
     return minima

def find_r(Ixx, Ixy, Iyy, y, x, off):
     k = 0.04
     offset = int (off)
     windowIxx = Ixx[y-offset:y+offset+1, x-offset:x+offset+1]
     windowIxy = Ixy[y-offset:y+offset+1, x-offset:x+offset+1]
     windowIyy = Iyy[y-offset:y+offset+1, x-offset:x+offset+1]
     Sxx = windowIxx.sum()
     Sxy = windowIxy.sum()
     Syy = windowIyy.sum()
     if ((math.pow(Sxx + Syy, 2)) - (4 * ((Sxx * Syy) - Sxy)) < 0):
          return 0
     else:
          lambda1 = ((Sxx + Sxy) + math.sqrt((math.pow(Sxx + Syy, 2)) - (4 * ((Sxx * Syy) - Sxy))))/2
          lambda2 = ((Sxx + Sxy) - math.sqrt((math.pow(Sxx + Syy, 2)) - (4 * ((Sxx * Syy) - Sxy))))/2
          det = lambda1 * lambda2
          trace = lambda1 + lambda2
          r = det - (k*(trace**2))
          return r
     

def cari_daerah_x(id_x):
     if (id_x >= 0) and (id_x <= 3):
          return 0
     elif (id_x >= 4) and (id_x <= 7):
          return 1
     elif (id_x >= 8) and (id_x <= 11):
          return 2
     elif (id_x >= 12) and (id_x <= 15):
          return 3

def cari_daerah_y(id_y):
     if (id_y >= 0) and (id_y <= 3):
          return 0
     elif (id_y >= 4) and (id_y <= 7):
          return 4
     elif (id_y >= 8) and (id_y <= 11):
          return 8
     elif (id_y >= 12) and (id_y <= 15):
          return 12

def cari_index_histogram(turunan_x, turunan_y, atan):
     if (turunan_x > 0) and (turunan_y > 0):
          deg = math.degrees(atan)
          if (deg >= 0) and (deg < 45):
               return 0
          elif (deg >= 45) and (deg <= 90):
               return 1
     elif (turunan_x < 0) and (turunan_y > 0):
          deg = 180 + math.degrees(atan)
          if (deg >= 90) and (deg < 135):
               return 2
          elif (deg >= 135) and (deg <= 180):
               return 3
     elif (turunan_x < 0) and (turunan_y < 0):
          deg = 180 + math.degrees(atan)
          if (deg >= 180) and (deg < 225):
               return 4
          elif (deg >= 225) and (deg <= 270):
               return 5
     elif (turunan_x > 0) and (turunan_y < 0):
          deg = 360 + math.degrees(atan)
          if (deg >= 270) and (deg < 315):
               return 6
          elif (deg >= 315) and (deg <= 360):
               return 7
     else:
          return 0

def find_keypoint(atas, tengah, bawah, L, img, size):
     hasil = img.copy()
     tinggi = tengah.shape[0]
     lebar = tengah.shape[1]
     offset = (size-1)/2
     Ix = cv2.Sobel(tengah, cv2.CV_64F, 1, 0, ksize=size)
     Iy = cv2.Sobel(tengah, cv2.CV_64F, 0, 1, ksize=size)
     Ixx = Ix**2
     Ixy = Ix*Iy
     Iyy = Iy**2
     keypoint = []
     kp = []
     for y in range(1, tinggi-1):
          for x in range(1, lebar-1):
               tetangga_atas = atas[y-1:y+2, x-1:x+2]
               tetangga_tengah = tengah[y-1:y+2, x-1:x+2]
               tetangga_bawah = bawah[y-1:y+2, x-1:x+2]
               #maxima = local_maxima(tetangga_atas, tetangga_tengah, tetangga_bawah)
               if __name__ == "__main__":
                   p=Pool(4)
                   maxima=p.map(multi_run_wrapper, [(tetangga_atas, tetangga_tengah, tetangga_bawah)])
                   return maxima
               minima = local_minima(tetangga_atas, tetangga_tengah, tetangga_bawah)
               if (maxima == True) or (minima == True):
                    ix = tengah[y+1][x] - tengah[y-1][x]
                    iy = tengah[y][x+1] - tengah[y][x-1]
                    mag = math.sqrt(ix**2 + iy**2)
                    if mag > 0.012:
                         r = find_r(Ixx, Ixy, Iyy, y, x, offset)
                         if (r > 10):
                              temp = []
                              key = [0, 0, 0, 0, 0, 0, 0, 0]
                              for i in range(16):
                                   temp.append([0, 0, 0, 0, 0, 0, 0, 0])
                              cv2.circle(hasil, (x, y), 2, (0, 255, 0), -1)
                              id_y = 0
                              for index_y in range(y-8, y+8, 1):
                                   id_x = 0
                                   for index_x in range(x-8, x+8, 1):
                                        daerah = cari_daerah_x(id_x)
                                        daerah += cari_daerah_y(id_y)
                                        turunan_x = L[index_y][index_x+1] - L[index_y][index_x-1]
                                        turunan_y = L[index_y-1][index_x] - L[index_y+1][index_x]
                                        tan = turunan_y
                                        if turunan_x != 0:
                                             tan = turunan_y / turunan_x
                                        sudut = math.atan(tan)
                                        index_histogram = cari_index_histogram(turunan_x, turunan_y, sudut)
                                        temp[daerah][index_histogram] += 1
                                        key[index_histogram] += 1
                                        id_x += 1
                                   id_y += 1
                              keypoint.append(temp)
                              kp.append(key)
                         else:
                              tengah[y][x] = 0
                    else:
                         tengah[y][x] = 0
               else:
                    tengah[y][x] = 0
     return hasil, keypoint, kp

ksize = 5
ksize1 = 5
k = math.sqrt(2)
sigma = k/2
img = cv2.imread('datatest outdoor/' + str(2) + '.jpg')

height, width = img.shape[:2]

img = cv2.resize(img, (int(width/5), int (height/5)), interpolation = cv2.INTER_CUBIC)

img1 = img.copy()
img2 = cv2.resize(img1, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
img3 = cv2.resize(img2, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)
img4 = cv2.resize(img3, None, fx=0.5, fy=0.5, interpolation = cv2.INTER_CUBIC)

# Convert Gray Scale
level_1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
level_2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
level_3 = cv2.cvtColor(img3, cv2.COLOR_BGR2GRAY)
level_4 = cv2.cvtColor(img4, cv2.COLOR_BGR2GRAY)

# Normalisasi
level_1 = cv2.normalize(level_1, level_1.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
level_2 = cv2.normalize(level_2, level_2.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
level_3 = cv2.normalize(level_3, level_3.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
level_4 = cv2.normalize(level_4, level_4.shape, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

# Gaussian Kernel
GaussianKernel0 = cv2.getGaussianKernel(ksize, sigma, cv2.CV_32F)
GaussianKernel1 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 1), cv2.CV_32F)
GaussianKernel2 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 2), cv2.CV_32F)
GaussianKernel3 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 3), cv2.CV_32F)
GaussianKernel4 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 4), cv2.CV_32F)
GaussianKernel5 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 5), cv2.CV_32F)
GaussianKernel6 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 6), cv2.CV_32F)
GaussianKernel7 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 7), cv2.CV_32F)
GaussianKernel8 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 8), cv2.CV_32F)
GaussianKernel9 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 9), cv2.CV_32F)
GaussianKernel10 = cv2.getGaussianKernel(ksize, sigma*math.pow(k, 10), cv2.CV_32F)
GaussianKernel = cv2.getGaussianKernel(ksize, sigma*1.5, cv2.CV_32F)

gauss1 = cv2.filter2D(level_1, -1, GaussianKernel)
gauss2 = cv2.filter2D(level_2, -1, GaussianKernel)
gauss3 = cv2.filter2D(level_3, -1, GaussianKernel)
gauss4 = cv2.filter2D(level_4, -1, GaussianKernel)

# Gauss Image Level 1
gauss1_level1 = cv2.filter2D(level_1, -1, GaussianKernel0)
gauss2_level1 = cv2.filter2D(level_1, -1, GaussianKernel1)
gauss3_level1 = cv2.filter2D(level_1, -1, GaussianKernel2)
gauss4_level1 = cv2.filter2D(level_1, -1, GaussianKernel3)
gauss5_level1 = cv2.filter2D(level_1, -1, GaussianKernel4)

# Gauss Image Level 2
gauss1_level2 = cv2.filter2D(level_2, -1, GaussianKernel2)
gauss2_level2 = cv2.filter2D(level_2, -1, GaussianKernel3)
gauss3_level2 = cv2.filter2D(level_2, -1, GaussianKernel4)
gauss4_level2 = cv2.filter2D(level_2, -1, GaussianKernel5)
gauss5_level2 = cv2.filter2D(level_2, -1, GaussianKernel6)

# Gauss Image Level 3
gauss1_level3 = cv2.filter2D(level_3, -1, GaussianKernel4)
gauss2_level3 = cv2.filter2D(level_3, -1, GaussianKernel5)
gauss3_level3 = cv2.filter2D(level_3, -1, GaussianKernel6)
gauss4_level3 = cv2.filter2D(level_3, -1, GaussianKernel7)
gauss5_level3 = cv2.filter2D(level_3, -1, GaussianKernel8)

# Gauss Image Level 4
gauss1_level4 = cv2.filter2D(level_4, -1, GaussianKernel6)
gauss2_level4 = cv2.filter2D(level_4, -1, GaussianKernel7)
gauss3_level4 = cv2.filter2D(level_4, -1, GaussianKernel8)
gauss4_level4 = cv2.filter2D(level_4, -1, GaussianKernel9)
gauss5_level4 = cv2.filter2D(level_4, -1, GaussianKernel10)

# DoG Level 1
dog1_level1 = level_1.copy()
dog2_level1 = level_1.copy()
dog3_level1 = level_1.copy()
dog4_level1 = level_1.copy()
dog5_level1 = level_1.copy()
for y in range(dog1_level1.shape[0]):
     for x in range(dog1_level1.shape[1]):
          dog1_level1[y][x] = gauss1_level1[y][x] - gauss2_level1[y][x]
          dog2_level1[y][x] = gauss2_level1[y][x] - gauss3_level1[y][x]
          dog3_level1[y][x] = gauss3_level1[y][x] - gauss4_level1[y][x]
          dog4_level1[y][x] = gauss4_level1[y][x] - gauss5_level1[y][x]

# DoG Level 2
dog1_level2 = level_2.copy()
dog2_level2 = level_2.copy()
dog3_level2 = level_2.copy()
dog4_level2 = level_2.copy()
dog5_level2 = level_2.copy()
for y in range(dog1_level2.shape[0]):
     for x in range(dog1_level2.shape[1]):
          dog1_level2[y][x] = gauss1_level2[y][x] - gauss2_level2[y][x]
          dog2_level2[y][x] = gauss2_level2[y][x] - gauss3_level2[y][x]
          dog3_level2[y][x] = gauss3_level2[y][x] - gauss4_level2[y][x]
          dog4_level2[y][x] = gauss4_level2[y][x] - gauss5_level2[y][x]

# DoG Level 3
dog1_level3 = level_3.copy()
dog2_level3 = level_3.copy()
dog3_level3 = level_3.copy()
dog4_level3 = level_3.copy()
dog5_level3 = level_3.copy()
for y in range(dog1_level3.shape[0]):
     for x in range(dog1_level3.shape[1]):
          dog1_level3[y][x] = gauss1_level3[y][x] - gauss2_level3[y][x]
          dog2_level3[y][x] = gauss2_level3[y][x] - gauss3_level3[y][x]
          dog3_level3[y][x] = gauss3_level3[y][x] - gauss4_level3[y][x]
          dog4_level3[y][x] = gauss4_level3[y][x] - gauss5_level3[y][x]

# DoG Level 4
dog1_level4 = level_4.copy()
dog2_level4 = level_4.copy()
dog3_level4 = level_4.copy()
dog4_level4 = level_4.copy()
dog5_level4 = level_4.copy()
for y in range(dog1_level4.shape[0]):
     for x in range(dog1_level4.shape[1]):
          dog1_level4[y][x] = gauss1_level4[y][x] - gauss2_level4[y][x]
          dog2_level4[y][x] = gauss2_level4[y][x] - gauss3_level4[y][x]
          dog3_level4[y][x] = gauss3_level4[y][x] - gauss4_level4[y][x]
          dog4_level4[y][x] = gauss4_level4[y][x] - gauss5_level4[y][x]

list_keypoint = []
print(1)
k1, c1, kp1 = find_keypoint(dog1_level1, dog2_level1, dog3_level1, gauss1, img1, ksize1)
list_keypoint.append(c1)
print(2)
k2, c2, kp2 = find_keypoint(dog2_level1, dog3_level1, dog4_level1, gauss1, img1, ksize1)
list_keypoint.append(c2)
print(3)
k3, c3, kp3 = find_keypoint(dog1_level2, dog2_level2, dog3_level2, gauss2, img2, ksize1)
list_keypoint.append(c3)
print(4)
k4, c4, kp4 = find_keypoint(dog2_level2, dog3_level2, dog4_level2, gauss2, img2, ksize1)
list_keypoint.append(c4)
print(5)
k5, c5, kp5 = find_keypoint(dog1_level3, dog2_level3, dog3_level3, gauss3, img3, ksize1)
list_keypoint.append(c5)
print(6)
k6, c6, kp6 = find_keypoint(dog2_level3, dog3_level3, dog4_level3, gauss3, img3, ksize1)
list_keypoint.append(c6)
print(7)
k7, c7, kp7 = find_keypoint(dog1_level4, dog2_level4, dog3_level4, gauss4, img4, ksize1)
list_keypoint.append(c7)
print(8)
k8, c8, kp8 = find_keypoint(dog2_level4, dog3_level4, dog4_level4, gauss4, img4, ksize1)
list_keypoint.append(c8)
test = list_keypoint

index = 0
best_match = 0
persen_match = 0
for img in train:
     print(" =========== "+str(index)+" =============== ")
     banyak_kp = 0
     banyak_kp_match = 0
     for im in img[1]:
          for kp in im:
               minn = 1000
               banyak_kp += 1
               for im2 in test:
                    for kp2 in im2:
                         tot = 0
                         h = np.abs(np.array(kp) - np.array(kp2))
                         for x in h:
                              for y in x:
                                   tot += y
                         if tot < minn:
                              minn = tot
               if minn <= 200:
                    banyak_kp_match += 1
     banyak_kp_test = 0
     for i in test:
          banyak_kp_test += len(i)
     bagi = abs(banyak_kp-banyak_kp_test)
     if bagi == 0:
          bagi += 1
     persen = banyak_kp_match/bagi
     print(persen)
     if persen > persen_match:
          best_match = index
          persen_match = persen
     index += 1

cv2.imshow("c1", k1)
cv2.imshow("c2", k2)
cv2.imshow("c3", k3)
cv2.imshow("c4", k4)
cv2.imshow("c5", k5)
cv2.imshow("c6", k6)
cv2.imshow("c7", k7)
cv2.imshow("c8", k8)

gc = client.GoodreadsClient("ocezYtOZLhHQHoWuaOE4w", "gMGUvirhqfWX8L02pBvdbui3iWX5jvRIeiElFmcHGMw")
book = gc.book(train[best_match][0])
title = book.title
rating = book.average_rating

print(title)

end = timer()
print("Time : ", end-start, " Second")

cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.waitKey(1)
